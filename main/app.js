function distance(first, second){
	if(first === undefined || first.length == 0)
	{
		if(second === undefined || second.length == 0)
			return 0;
	}
	else
	{
		var areBothArrays = false;
		if(Array.isArray(first) && Array.isArray(second))
			areBothArrays=true;
		else
			throw new Error("InvalidType");

		var firstSet = new Set(first);
		var firstArray = [...firstSet];
		var secondSet = new Set(second);
		var secondArray = [...secondSet];

		let difference = firstArray.filter(x => !secondArray.includes(x))
		.concat(secondArray.filter(x => !firstArray.includes(x)))

		var distance = difference.length;

		return distance;
	}

	

}
array1 = [1, 1, 2, 3, 4, 4];
array2 = [1, 1, 10, 11, 4, 1];
var theDistance = distance(array1, array2);
console.log("The distance between the two arrays is: ", theDistance);


module.exports.distance = distance